<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimedItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claimed_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('itemid')->unsigned();
            $table->foreign('itemid')->references('id')->on('items');
            $table->integer('claimid')->unsigned();
            $table->foreign('claimid')->references('id')->on('claims');
            $table->integer('adminid')->unsigned();
            $table->foreign('adminid')->references('id')->on('users');
            $table->integer('claimersuid');
            $table->integer('userid')->unsigned();
            $table->foreign('userid')->references('id')->on('users');
            $table->softDeletes();
            $table->integer('status');
            $table->string('destination');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claimed_items');
    }
}
