<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('primarycolorid')->unsigned();
            $table->foreign('primarycolorid')->references('id')->on('colors');
            $table->integer('secondarycolorid')->unsigned();
            $table->foreign('secondarycolorid')->references('id')->on('colors');
            $table->integer('areaid')->unsigned();
            $table->foreign('areaid')->references('id')->on('areas');
            $table->integer('userid')->unsigned();
            $table->foreign('userid')->references('id')->on('administrators');
            $table->string('model');
            $table->integer('subcategory');
            $table->date('datelost');
            $table->string('brand')->nullable();
            $table->string('serialnumber')->nullable();
            $table->string('claimid')->nullable();
            $table->text('description');
            // Altered finder SUID from integer to string
            $table->string('findersuid');
            $table->string('finderphonenumber');
            $table->string('finderemail');
            $table->softDeletes();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
