<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand')->nullable();
            $table->integer('primarycolorid')->unsigned();
            $table->foreign('primarycolorid')->references('id')->on('colors');
            $table->integer('secondarycolorid')->unsigned();
            $table->foreign('secondarycolorid')->references('id')->on('colors');
            $table->integer('areaid')->unsigned();
            $table->foreign('areaid')->references('id')->on('areas');
            $table->string('studentid');
            $table->integer('subcategory');
            $table->string('model');
            $table->string('serialnumber');
            $table->text('description');
            $table->string('claimersuid');
            $table->string('claimerphonenumber');
            $table->string('claimeremail');
            $table->softDeletes();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
