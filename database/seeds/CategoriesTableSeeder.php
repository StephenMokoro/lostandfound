<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $data = [
             ['category' => 'Accessory','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //2
             ['category' => 'Book','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //3
             ['category' => 'Bag','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //4
             ['category' => 'Clothing','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //5
             ['category' => 'Currency','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //6
             ['category' => 'Electronics','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()],  //7
             ['category' => 'Eye Wear','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //8
             ['category' => 'Foot Wear','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //9
             ['category' => 'Identification','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //10
             ['category' => 'Jewelery','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //11
             ['category' => 'Keys','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //12
             ['category' => 'Medical Equipment','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //13
             ['category' => 'Miscellaneous','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //14
             ['category' => 'Musical Instrument','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //15
             ['category' => 'Sports Equipment','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //16
             ['category' => 'Wallet/Purse','status' => '34', 'userid' => '1', 'created_at' => Carbon::now()], //17

         ];

         DB::table('categories')->insert($data);
    }
}
