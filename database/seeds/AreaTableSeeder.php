<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      $data = [
        // Phase 1
        ['area' => 'RM 01','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'RM 02','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'RM 03','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 11','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 12','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 13','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 14','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 21','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'RM 22','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 23','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 24','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 25','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 31','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 32','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Rm 33','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'RM 34','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 1','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 2','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 3','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 4','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 5','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'LT 6','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Chapel','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Blue Sky','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Elgon Lab','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Receiption','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Aberdere Lab','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SUSWA Lab','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Faculty of IT','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'School Of Accounting','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],


        // Strathmore Business School
        ['area' => 'SBS 1','status' => '34', 'locationid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SBS 2','status' => '34', 'locationid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SBS 3','status' => '34', 'locationid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SBS 4','status' => '34', 'locationid' => '2', 'userid' => '1', 'created_at' => Carbon::now()],

        // Auditorium
        ['area' => 'Basement','status' => '34', 'locationid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Graduation Square','status' => '34', 'locationid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Hall','status' => '34', 'locationid' => '3', 'userid' => '1', 'created_at' => Carbon::now()],

        // MSB
        ['area' => 'MSB 1','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 2','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 3','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 4','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 5','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 6','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 7','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 8','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 9','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 10','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 11','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 12','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'MSB 13','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Staff Room','status' => '34', 'locationid' => '4', 'userid' => '1', 'created_at' => Carbon::now()],

        // LSB
        ['area' => 'GF - 01','status' => '34', 'locationid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'GF - 02','status' => '34', 'locationid' => '5', 'userid' => '1', 'created_at' => Carbon::now()],

        // Law School
        ['area' => 'SLS 1','status' => '34', 'locationid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SLS 2','status' => '34', 'locationid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SLS 3','status' => '34', 'locationid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'SLS 4','status' => '34', 'locationid' => '6', 'userid' => '1', 'created_at' => Carbon::now()],

        // Graduation Square


        // Students Center
        ['area' => 'Cafeteria','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Writing Center','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Music Room','status' => '34', 'locationid' => '1', 'userid' => '1', 'created_at' => Carbon::now()],

        ['area' => 'iLab','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'iBiz','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'First Floor','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Second Floor','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Third Floor','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Fourth Floor','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Fifth Floor','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],
        ['area' => 'Club Office','status' => '34', 'locationid' => '8', 'userid' => '1', 'created_at' => Carbon::now()],


      ];

        DB::table('areas')->insert($data);
    }
}
