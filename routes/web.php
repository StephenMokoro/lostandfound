<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


// Route to display the Index page
Route::get('/', 'WelcomeController@getIndex');

Route::get('visitor/claim', 'RoutesController@getVisitorClaim')->name('visitor.claim');

Route::post('post/visitor/claim', 'RoutesController@postVisitorClaim')->name('post.visitor.claim');

Route::get('visitor/sulf/ajax/category/{category}', 'RoutesController@getSubcat');
Route::get('visitor/sulf/ajax/location/{location}', 'RoutesController@getArea');
Route::get('visitor/sulf/ajax/subcategory/{subcategory}', 'RoutesController@getSearchBrand');


Route::group(['middleware' => 'cas.auth'], function () {
    // Route to display the login page
    Route::get('userlogin', 'WelcomeController@getUserLogin');
    // Route to display the login page
    // Route::post('studentlogin', 'WelcomeController@postStudentLogin');

    // Route to display the signup page
    // Route::get('adminlogin', 'WelcomeController@getAdminLogin');

    Route::get('user/logout', 'Auth\LoginController@getLogout')->name('user.logout');

    // Route::any('{all}', function(){ return view('Welcome.welcome'); })->where('all', '.*');

    // Route to display the user Dashboard
    Route::get('dash', 'RoutesController@getUserDash');



    // Route to display admin routes
    Route::resource('claim', 'ClaimController');

    // Route to display the rejected claims
    Route::get('rejected', 'RoutesController@getRejectedUserClaims');

    // Route to display the Claimed claims
    Route::get('reclaimed', 'RoutesController@getUserReclaimed');

    // Route to display the Claimed claims
    Route::get('deletedclaims', 'RoutesController@getDeletedClaims');

    // Route to display the
    Route::get('pending', 'RoutesController@getPendingUserClaims')->name('pending.index');


    Route::group(['middleware' => 'admin'], function () {

    Route::get('items/approved', [
        'as' => 'reclaimer.claim',
        'uses' => 'RoutesController@claim'
    ]);

    Route::get('claim/{claimid}/item/{itemid}', [
        'as' => 'reclaimed.items',
        'uses' => 'RoutesController@claimant'
    ]);

    Route::get('delete/claim/{claimid}', [
        'as' => 'removependingclaim',
        'uses' => 'RoutesController@removependingclaim'
    ]);

    Route::get('contribute/item/{itemid}', [
        'as' => 'contributeitem',
        'uses' => 'RoutesController@contributeitem'
    ]);
    Route::get('contributed', [
        'as' => 'contributeditemslist',
        'uses' => 'RoutesController@contributeditems'
    ]);

    // Route to display the admin Dashboard
    Route::get('admindash', 'RoutesController@getAdminDash')->name('admin');

    // Route to display the admin Dashboard
    Route::get('settings', 'RoutesController@getSettings');

    // Route to display the admin Dashboard
    Route::get('pendingclaims', 'RoutesController@getPendingClaims');

    // Route to display admin routes
    Route::resource('rejectedclaims', 'RoutesController@getRejectedClaims');

    // Route to display admin routes
    Route::get('reclaimeditems', 'RoutesController@getReclaimed');

    // Route to display admin routes
    Route::resource('registeredadmins', 'RoutesController@getRegisteredAdmins');

    // Route to display admin routes
    Route::resource('admins', 'AdminController');

    // Route to display category routes
    Route::resource('category', 'CategoryController');

    // Route to display admin routes
    Route::resource('subcategory', 'SubCategoryController');

    // Route to display admin routes
    Route::resource('brand', 'BrandController');

    // Route to display admin routes
    Route::resource('color', 'ColorController');

    // Route to display admin routes
    Route::resource('location', 'LocationController');

    // Route to display admin routes
    Route::resource('area', 'AreaController');

    // Route to display admin routes
    Route::resource('item', 'ItemController');


});


    // Route::get('visitor/claim', 'RoutesController@getVisitorClaim')->name('visitor.claim');

    Route::get('/home', 'HomeController@index');

    Route::get('sulf/ajax/category/{category}', 'RoutesController@getSubcat');
    Route::get('sulf/ajax/location/{location}', 'RoutesController@getArea');
    Route::get('sulf/ajax/subcategory/{subcategory}', 'RoutesController@getSearchBrand');

});
