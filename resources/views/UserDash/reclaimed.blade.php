@extends('UserDash.main')

@section('title')
Pending Claims
@endsection()


@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Succeseful Claims
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Serial Number</th>
                              <th>Brand</th>
                              <th>Model</th>
                              <th>Primary Color</th>
                              <th>Secondary Color</th>
                              <th>Location</th>
                              <th>Area</th>
                              <th>Registered</th>

                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Claim::where('status', 99)->where('claimersuid', $value)->get(); as $claims)
                            <tr>
                              <td>{{$claims->serialnumber}}</td>
                              <td>
                                {{
                                  $userval = DB::table('brands')->where('id', $claims->brandid)->value('brand')
                                }}
                              </td>
                                <td>{{$claims->model}}</td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $claims->primarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $claims->secondarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                <td>
                                  {{
                                    $userval = DB::table('locations')->where('id', DB::table('areas')->where('id', $claims->areaid)->value('locationid'))->value('location')
                                  }}
                                </td>
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('areas')->where('id', $claims->areaid)->value('area')
                                  }}
                              </td>
                              <td>{{$claims->created_at}}</td>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->

        <footer>
          <div class="copyright-info">
            <p class="pull-right">Strathmore University Lost & Found <a href="http://www.strathmore.edu/en/"></a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
