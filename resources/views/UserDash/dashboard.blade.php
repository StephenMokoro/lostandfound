@extends('UserDash.main')

@section('title')
Dashboard
@endsection()



@section('content')
      <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Pending Claims</span>
              <div class="count">{{$pending}}</div>
              <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-user"></i> Total Approved Claims</span>
              <div class="count">{{$reclaimed}}</div>
              <!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
            </div>
          </div>

        </div>
        <!-- /top tiles -->
        <br />


        <!-- footer content -->

        <footer>
          <div class="copyright-info">
            <p class="pull-right">Strathmore University Lost & Found <a href="http://www.strathmore.edu/en/"></a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
