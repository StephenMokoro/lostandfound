@extends('UserDash.main')

@section('title')
Claim Form
@endsection()


@section('content')

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Lost Item Registration Form</h3>
              <h5>PLEASE FILL ALL FIELDS</h5>
            </div>

          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">

                  @if($errors->any())
                    <div class="alert alert-danger">
                      @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                      @endforeach
                    </div>
                  @endif
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  {!!  Form::open(['route' => 'claim.store']) !!}
                  <div id="demo-form2"  class="form-horizontal form-label-left">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Category *</label>
                        <div class="col-md-9 col-sm-9">
                            <select name="category" id="category" class="form-control">
                              @if(old('category') != null)
                                  <option value="{{old('category')}}">{{\App\Category::find((old('category')))->category}}</option>
                              @endif
                              <option value="">Select Category...</option>
                              @foreach(\App\Category::all() as $categories)
                                  <option value="{{($categories->id)}}">{{$categories->category}}</option>
                              @endforeach()
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">SubCategory *</label>
                        <div class="col-md-9 col-sm-9">
                            <select name="subcategory" id="subcategory" class="form-control">
                              @if(old('subcategory') != null)
                                  <option value="{{old('subcategory')}}">{{\App\SubCategories::find((old('subcategory')))->category}}</option>
                              @endif
                              <option value="">Select SubCategory...</option>
                            </select>
                        </div>
                        <span class="help-block" id="subategoryerror">
                          @if ($errors->has('subcategory')) {{$errors->first('subcategory')}} @endif
                      </span>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3" for="last-name">Brand
                        </label>
                        <div class="col-md-9 col-sm-9">
                          <input type="text" id="brand" name="brand" class="form-control col-md-7">
                        </div>
                        @if ($errors->has('brand'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('brand') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3" for="last-name">Model/Name
                        </label>
                        <div class="col-md-9 col-sm-9">
                          <input type="text" id="model" name="model" class="form-control col-md-7">
                        </div>
                        @if ($errors->has('model'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('model') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3" for="last-name">Serial Number
                        </label>
                        <div class="col-md-9 col-sm-9">
                          <input type="text" id="serialnumber" name="serialnumber" class="form-control col-md-9">
                        </div>
                        @if ($errors->has('serialnumber'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('serialnumber') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Primary Color *</label>
                        <div class="col-md-9 col-sm-9">
                          <select name="primarycolor" id="" class="form-control">
                            @if(old('primarycolor') != null)
                                <option value="{{old('primarycolor')}}">{{\App\Color::find(decrypt(old('primarycolor')))->primarycolor}}</option>
                            @endif
                              <option value="">Select Color...</option>
                              @foreach(\App\Color::all() as $colors)
                                  <option value="{{encrypt($colors->id)}}">{{$colors->color}}</option>
                              @endforeach()
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Secondary Color *</label>
                        <div class="col-md-9 col-sm-9">
                          <select name="secondarycolor" id="" class="form-control">
                            @if(old('secondarycolor') != null)
                                <option value="{{old('secondarycolor')}}">{{\App\Color::find(decrypt(old('secondarycolor')))->secondarycolor}}</option>
                            @endif
                              <option value="">Select Color...</option>
                              @foreach(\App\Color::all() as $colors)
                                  <option value="{{encrypt($colors->id)}}">{{$colors->color}}</option>
                              @endforeach()
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Description *</label>
                        <div class="col-md-9 col-sm-9">
                          <textarea name="description" class="resizable_textarea form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                        </div>
                        @if ($errors->has('description'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('description') }}</strong>
            							</span>
            					  @endif
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3">Claimer SUID</label>
                        <div class="col-md-9 col-sm-9">
                          <input id="findersuid" class="form-control col-md-7" readonly="readonly" value="{{$value}}" type="text" name="claimersuid">
                        </div>
                        @if ($errors->has('claimersuid'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('claimersuid') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Claimer Phone number *</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input name="claimerphonenumber" type="text" class="form-control" data-inputmask="'mask' : '+(254) 999-999-999'">
                          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        @if ($errors->has('claimerphonenumber'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('claimerphonenumber') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Claimer Email *</label>
                         <div class="col-md-9 col-sm-9">
                          <input id="finderemail" class="form-control col-md-7" type="email" name="claimeremail">
                        </div>
                        @if ($errors->has('claimeremail'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('claimeremail') }}</strong>
            							</span>
            					  @endif
                      </div>
                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Location *</label>
                        <div class="col-md-9 col-sm-9">
                          <select name="location" id="location" class="form-control">

                              <option value="">Select a Location...</option>
                              @foreach(\App\Location::all() as $locations)
                                  <option value="{{($locations->id)}}">{{$locations->location}}</option>
                              @endforeach()
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Area *</label>
                        <div class="col-md-9 col-sm-9">
                          <select name="area" id="area" class="form-control">
                              @if(old('area') != null)
                                  <option value="{{old('area')}}">{{\App\Area::find((old('area')))->area}}</option>
                              @endif
                              <option value="">Select an Area...</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Date Lost <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9">
                          <input id="birthday" name="datelost" class="date-picker form-control col-md-7" required="required" type="text">
                        </div>
                        @if ($errors->has('datelost'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('datelost') }}</strong>
            							</span>
            					  @endif
                      </div>
                    </div>
                    </div>
                    <br />
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-md-offset-5">
                        <button type="submit" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                endDate:'today',
                singleDatePicker: true,
                endDate: '+0d',
                calender_style: "picker_2"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>

<script>
$('#category').on('change',function (e) {
          var category = e.target.value;
          $.get('../sulf/ajax/category/'+category, function (data) {
              $('#subcategory').empty();
              $('#subcategory').append('<option value="">Select SubCategory</option>');
              $('#subcategory').append('<option value="">---------------</option>');
              $.each(data, function (data, subcategory) {
                  $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
              })
          });
      });
</script>
<script>
$('#subcategory').on('change',function (e) {
          var subcategory = e.target.value;
          $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
              $('#brand').empty();
              $('#brand').append('<option value="">Select Brand</option>');
              $('#brand').append('<option value="">---------------</option>');
              $.each(data, function (data, brand) {
                  $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
              })
          });
      });
</script>
<script>
$('#location').on('change',function (e) {
          var location = e.target.value;
          $.get('../sulf/ajax/location/'+location, function (data) {
              $('#area').empty();
              $('#area').append('<option value="">Select Areas</option>');
              $('#area').append('<option value="">---------------</option>');
              $.each(data, function (data, area) {
                  $('#area').append('<option value="'+area.id+'">'+area.area+'</option>')
              })
          });
      });
</script>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Strathmore University Lost & Found by <a href="http://www.strathmore.edu/en/">Strathmore University</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

    @endsection()
