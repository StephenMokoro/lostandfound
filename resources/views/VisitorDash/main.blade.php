<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>SULF | @yield('title')</title>

  <!-- Bootstrap core CSS -->
  @include('UserDash.partials._header')

</head>

<body class="nav-md">
  <div class="container body">

    <div class="main_container">




      @yield('content')
      <!-- page content -->


      <!-- /page content -->

    </div>

  </div>

@include('VisitorDash.partials._footer')

</body>

</html>
