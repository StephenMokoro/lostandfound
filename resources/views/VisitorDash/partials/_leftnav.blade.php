<div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"><i class="fa fa-binoculars"></i> <span>SULF</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="{{asset('/Gentella/images/img.jpg')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{ $value }}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br />


          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3>General</h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('dash')}}">Dashboard</a>
                    </li>
                    <li>
                      @php($user =  Cas::user() )
                    @if(\App\Administrator::where('username',$user)->first())
                        <a class="page-scroll" href="{{route('admin')}}">Admin Dash</a>
                    @endif
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-edit"></i> Claim <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('claim/create')}}">Claim</a>
                    </li>
                  </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> Claims <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{url('reclaimed')}}"> Successfully Claimed</a>
                    </li>
                    <li><a href="{{url('pending')}}"> Pending</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->

        </div>
      </div>
