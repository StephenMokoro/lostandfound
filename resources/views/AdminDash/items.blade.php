@extends('AdminDash.main')

@section('title')
Registered Items
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Items
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <span class="">
                          <a href="{{route('item.create')}}" class="btn btn-success btn-block">Register Item!</a>
                  </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Serial Number</th>
                              <th>Sub Category</th>
                              <th>Brand</th>
                              <th>Model</th>
                              <th>Primary Color</th>
                              <th>Secondary Color</th>
                              <th>Area</th>
                              <th>Registered</th>
                              <th>Registerer</th>
                              <th>Edit</th>
                              <th>Finder</th>
                              <th>Compare</th>
                              <th>Contribute</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Item::where('status', 34)->get(); as $items)
                            <tr>
                              <td>{{$items->serialnumber}}</td>
                              <td>
                                {{
                                  $userval = DB::table('sub_categories')->where('id', $items->subcategory)->value('subcategory')
                                }}
                              </td>
                              <td>{{$items->brand}}</td>
                                <td>{{$items->model}}</td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->secondarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('areas')->where('id', $items->areaid)->value('area')
                                  }}
                              </td>
                              <td>{{$items->created_at}}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $items->userid)->value('name')
                                }}</td>

                              <!-- Accept Modal -->
                  <div class="modal fade" id="{{$items->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Accept Modal</h4>
                        </div>
                        <div class="modal-body">
                          <form role="form">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Name</label>
                                <input type="text" readonly="readonly" value="{{$items->findersuid}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Phone Number</label>
                                <input type="text" readonly="readonly" value="{{$items->finderphonenumber}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Reason</label>
                                <textarea class="form-control" id="_reason" readonly="readonly" rows="5" placeholder="{{$items->description}}"></textarea>
                              </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                              <button type="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="edit{{$items->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Modal</h4>
                        </div>
                        <div class="modal-body">
                          <h2>WARNING! ALTERING THIS WILL AFFECT THIS ITEM</h2>
                          {{ Form::model($items, array('route' => array('item.update', encrypt($items->id)), 'method' => 'PUT')) }}
                          <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="box-body">



                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="">Category *</label>
                                    <div class="">
                                        <select name="category" id="category" class="form-control">
                                            <option value="{{
                                              $userval = DB::table('categories')->where('id', DB::table('sub_categories')->where('id', $items->subcategory)->value('categoryid'))->value('id')
                                            }}">{{
                                              $userval = DB::table('categories')->where('id', DB::table('sub_categories')->where('id', $items->subcategory)->value('categoryid'))->value('category')
                                            }}</option>
                                            @foreach(\App\Category::all() as $categories)
                                                <option value="{{($categories->id)}}">{{$categories->category}}</option>
                                            @endforeach()
                                        </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="">SubCategory *</label>
                                    <div class="">
                                        <select name="subcategory" id="subcategory" class="form-control">

                                          <option value="{{
                                            $userval = DB::table('sub_categories')->where('id', $items->subcategory)->value('id')
                                          }}">{{
                                            $userval = DB::table('sub_categories')->where('id', $items->subcategory)->value('subcategory')
                                          }}</option>
                                        </select>
                                    </div>
                                    <span class="help-block" id="subategoryerror">
                                      @if ($errors->has('subcategory')) {{$errors->first('subcategory')}} @endif
                                  </span>
                                  </div>

                                  <div class="form-group">
                                    <label class="" for="last-name">Brand
                                    </label>
                                    <div class="">
                                      <input type="text" id="brand" value="{{$items->brand}}" name="brand" class="form-control col-md-7">
                                    </div>
                                    @if ($errors->has('brand'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('brand') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                  <div class="form-group">
                                    <label class="" for="last-name">Model
                                    </label>
                                    <div class="">
                                      <input type="text" id="model" name="model" value="{{$items->model}}" class="form-control col-md-7">
                                    </div>
                                    @if ($errors->has('model'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('model') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                  <div class="form-group">
                                    <label class="" for="last-name">Serial Number
                                    </label>
                                    <div class="">
                                      <input type="text" id="serialnumber" value="{{$items->serialnumber}}" name="serialnumber" class="form-control col-md-9">
                                    </div>
                                    @if ($errors->has('serialnumber'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('serialnumber') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                  <div class="form-group">
                                    <label class="">Primary Color *</label>
                                    <div class="">
                                      <select name="primarycolor" id="" class="form-control">

                                          <option value="{{
                                            $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('id')
                                          }}">{{
                                            $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('color')
                                          }}</option>
                                          @foreach(\App\Color::all() as $colors)
                                              <option value="{{($colors->id)}}">{{$colors->color}}</option>
                                          @endforeach()
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="">Secondary Color *</label>
                                    <div class="">
                                      <select name="secondarycolor" id="" class="form-control">
                                          <option value="{{
                                            $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('id')
                                          }}">{{
                                            $userval = DB::table('colors')->where('id', $items->secondarycolorid)->value('color')
                                          }}</option>
                                          @foreach(\App\Color::all() as $colors)
                                              <option value="{{($colors->id)}}">{{$colors->color}}</option>
                                          @endforeach()
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="">Description *</label>
                                    <div class="">
                                      <textarea name="description" class=" form-control" style="width: 100%; overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;">{{$items->description}}

                                      </textarea>
                                    </div>
                                    @if ($errors->has('description'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('description') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="middle-name" class="">Finder Name *</label>
                                    <div class="">
                                      <input id="findersuid" value="{{$items->findersuid}}"  class="form-control col-md-7" type="text" name="findersuid">
                                    </div>
                                    @if ($errors->has('findersuid'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('findersuid') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                  <div class="form-group">
                                    <label class=" ">Finder Phone number *</label>
                                    <div class="">
                                      <input name="finderphonenumber" value="{{$items->finderphonenumber}}" type="text" class="form-control" data-inputmask="'mask' : '+(254) 999-999-999'">

                                    </div>
                                    @if ($errors->has('finderphonenumber'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('finderphonenumber') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <label class="">Location *</label>
                                    <div class="">
                                      <select name="location" id="location" class="form-control">
                                          @if(old('location') != null)
                                              <option value="{{old('location')}}">{{\App\Location::find((old('location')))->location}}</option>
                                          @endif
                                          <option value="{{
                                            $userval = DB::table('locations')->where('id', DB::table('areas')->where('id', $items->areaid)->value('locationid'))->value('id')
                                          }}">{{
                                            $userval = DB::table('locations')->where('id', DB::table('areas')->where('id', $items->areaid)->value('locationid'))->value('location')
                                          }}</option>
                                          @foreach(\App\Location::all() as $locations)
                                              <option value="{{($locations->id)}}">{{$locations->location}}</option>
                                          @endforeach()
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="">Area *</label>
                                    <div class="">
                                      <select name="area" id="area" class="form-control">
                                          @if(old('area') != null)
                                              <option value="{{old('area')}}">{{\App\Area::find((old('area')))->area}}</option>
                                          @endif
                                          <option value="{{
                                            $userval = DB::table('areas')->where('id', $items->areaid)->value('id')
                                          }}">{{
                                            $userval = DB::table('areas')->where('id', $items->areaid)->value('area')
                                          }}</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="">Date Lost <span class="required">*</span>
                                    </label>
                                    <div class="">
                                      <input id="birthday" name="datelost" value="{{$items->datelost}}" class="date-picker form-control col-md-7" required="required" type="text">
                                    </div>
                                    @if ($errors->has('datelost'))
                        						  <span class="help-block">
                        								<strong>{{ $errors->first('datelost') }}</strong>
                        							</span>
                        					  @endif
                                  </div>
                                </div>
                                </div>
                                <br />

                              </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                              <button type="submit" class="btn btn-block btn-danger">Edit</button>
                            </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                  <td><span class="btn btn-danger btn-block" data-toggle="modal" data-target="#edit{{$items->id}}">Edit</span></td>
                  <td><span class="btn btn-success btn-block" data-toggle="modal" data-target="#{{$items->id}}">More</span></td>
                  <td><a href="item/{{encrypt($items->id)}}" class="btn btn-success btn-danger">Compare</a></td>
                  <td><a href="contribute/item/{{encrypt($items->id)}}" class="btn btn-success btn-danger">Contribute</a></td>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />
        <script type="text/javascript">
          $(document).ready(function() {
            $('#birthday').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_4"
            }, function(start, end, label) {
              console.log(start.toISOString(), end.toISOString(), label);
            });
          });
        </script>
        <script>
        $('#category').on('change',function (e) {
                  var category = e.target.value;
                  $.get('../sulf/ajax/category/'+category, function (data) {
                      $('#subcategory').empty();
                      $('#subcategory').append('<option value="">Select SubCategory</option>');
                      $('#subcategory').append('<option value="">---------------</option>');
                      $.each(data, function (data, subcategory) {
                          $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.subcategory+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#subcategory').on('change',function (e) {
                  var subcategory = e.target.value;
                  $.get('../sulf/ajax/subcategory/'+subcategory, function (data) {
                      $('#brand').empty();
                      $('#brand').append('<option value="">Select Brand</option>');
                      $('#brand').append('<option value="">---------------</option>');
                      $.each(data, function (data, brand) {
                          $('#brand').append('<option value="'+brand.id+'">'+brand.brand+'</option>')
                      })
                  });
                  console.log();
              });
        </script>
        <script>
        $('#location').on('change',function (e) {
                  var location = e.target.value;
                  $.get('../sulf/ajax/location/'+location, function (data) {
                      $('#area').empty();
                      $('#area').append('<option value="">Select Areas</option>');
                      $('#area').append('<option value="">---------------</option>');
                      $.each(data, function (data, area) {
                          $('#area').append('<option value="'+area.id+'">'+area.area+'</option>')
                      })
                  });
                  console.log();
              });
        </script>

        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
