@extends('AdminDash.main')

@section('title')
Registered Admins
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Admins
                </h3>
            </div>

          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Username</th>
                              <th>SUID</th>
                              <th>Email</th>
                              <th>Registered</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Administrator::all(); as $administrators)
                            <tr>
                              <td>{{
                                $userval = DB::table('users')->where('id', $administrators->userid)->value('name')
                              }}</td>
                              <td>{{$administrators->suid}}</td>
                              <td>{{
                                $userval = DB::table('users')->where('id', $administrators->userid)->value('email')
                              }}</td>
                              <td>{{$administrators->created_at}}</td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$administrators->id}}">Delete</button></td>
                              <!-- Modal -->
                                <div id="delete{{$administrators->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                      </div>
                                      <div class="modal-body">
                                        <h2>WARNING! ALTERING THIS WILL DELETE THIS ADMINISTRATOR</h2>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['admins.destroy', encrypt($administrators->id)]
                                        ])  !!}
                                        {!! Form::token(); !!}
                                            {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
