@extends('AdminDash.main')

@section('title')
Registered Items
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Items
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <span class="">
                          <a href="item/create" class="btn btn-success btn-block">Register Item!</a>
                  </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">




          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Serial Number</th>
                              <th>Brand</th>

                              <th>Registerer</th>
                              <th>Finder</th>
                              <th>Compare</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Item::where('status', 34)->get(); as $items)
                            <tr>
                              <td>{{$items->serialnumber}}</td>
                              <td>{{$items->brand}}</td>
                                <td>{{$items->model}}</td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->primarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                {{
                                  $userval = DB::table('colors')->where('id', $items->secondarycolorid)->value('color')
                                }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('locations')
                                    ->join('areas', 'locations.id', '=', 'areas.locationid')
                                    ->value('locations.location')
                                  }}
                              </td>
                              <td>
                                  {{
                                    $userval = DB::table('areas')->where('id', $items->areaid)->value('area')
                                  }}
                              </td>
                              <td>{{$items->created_at}}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $items->userid)->value('name')
                                }}</td>

                              <!-- Accept Modal -->
                  <div class="modal fade" id="{{$items->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Accept Modal</h4>
                        </div>
                        <div class="modal-body">
                          <form role="form">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder SUID</label>
                                <input type="text" readonly="readonly" value="{{$items->findersuid}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Email</label>
                                <input type="text" readonly="readonly" value="{{$items->finderemail}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Finder Phone Number</label>
                                <input type="text" readonly="readonly" value="{{$items->finderphonenumber}}" class="form-control" id="_administrator" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Reason</label>
                                <textarea class="form-control" id="_reason" readonly="readonly" rows="3" placeholder="{{$items->description}}"></textarea>
                              </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                              <button type="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <td><span class="btn btn-success btn-block" data-toggle="modal" data-target="#{{$items->id}}">More</span></td>
                  <td><a href="item/{{encrypt($items->id)}}" class="btn btn-success btn-block">Compare</a></td>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
