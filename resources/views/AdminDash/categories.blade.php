@extends('AdminDash.main')

@section('title')
Registered Categories
@endsection()

@section('content')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                    Registered Categories
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <span class="">
                          <a href="{{route('category.create')}}" class="btn btn-success btn-block">Register Category!</a>
                  </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

        <div class="row">


          <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2> </h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <p class="text-muted font-13 m-b-30">

                        </p>
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Category</th>
                              <th>Registerer</th>
                              <th>Registered</th>
                              <th>Update</th>
                              <th>Delete</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach (\App\Category::all(); as $category)
                            <tr>
                              <td>{{$category->category}}</td>
                              <td>
                                {{
                                  $userval = DB::table('users')->where('id', $category->userid)->value('name')
                                }}</td>
                              <td>{{$category->created_at}}</td>
                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$category->id}}">Update</button></td>
                              <!-- Modal for Update -->
                              <div id="{{$category->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Update Category</h4>
                                    </div>
                                    <div class="modal-body">
                                      <h2>WARNING! ALTERING THIS WILL AFFECT ALL SUBCATEGORIES, BRANDS, CLAIMS AND ITEMS REGISTERED UNDER THIS SUBCATEGORY</h2>
                                      <br>
                                      <br>
                                      {{ Form::model($category, array('route' => array('category.update', encrypt($category->id)), 'method' => 'PUT')) }}
                                      <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                      <div class="row">
                                        <div class="col-md-12 ">

                                          <div class="form-group">
                                            <div class="col-md-12">
                                              <input type="text" id="subcategory" placeholder="Category" name="category" value="{{$category->category}}" required="required" class="form-control col-md-7">
                                            </div>
                                            @if ($errors->has('category'))
                                						  <span class="help-block">
                                								<strong>{{ $errors->first('category') }}</strong>
                                							</span>
                                					  @endif
                                          </div>
                                        </div>
                                        </div>
                                        <br />
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                          <div class="col-md-12 col-sm-6">
                                            <button type="submit" class="btn btn-success btn-block">Submit</button>
                                          </div>
                                        </div>

                                      </div>
                                      {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>

                                </div>
                              </div>

                              <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$category->id}}">Delete</button></td>
                              <!-- Modal -->
                                <div id="delete{{$category->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                      </div>
                                      <div class="modal-body">
                                        <h2>WARNING! ALTERING THIS WILL AFFECT ALL SUBCATEGORIES, BRANDS, CLAIMS AND ITEMS REGISTERED UNDER THIS SUBCATEGORY</h2>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['category.destroy', encrypt($category->id)]
                                        ])  !!}
                                        {!! Form::token(); !!}
                                            {!! Form::submit('Delete?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                            </tr>
                            @endforeach()
                          </tbody>
                        </table>

                      </div>
                    </div>
                  </div>

        </div>
        <br />


        <!-- footer content -->

        @include('AdminDash.partials._footnote')
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    @endsection()
