<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>SULF | @yield('title')</title>

  <!-- Bootstrap core CSS -->
  @include('AdminDash.partials._header')

</head>

<body class="nav-md">
  <div class="container body">

    <div class="main_container">


      @include('AdminDash.partials._leftnav')
      @include('AdminDash.partials._topnav')
      @include('AdminDash.partials._messages')


      @yield('content')
      <!-- page content -->

      <!-- /page content -->

    </div>

  </div>

@include('AdminDash.partials._footer')

</body>

</html>
