@extends('AdminDash.main')

@section('title')
Color Registration
@endsection()

@section('content')

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Color Registration Form</h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  {!!  Form::open(['route' => 'color.store']) !!}
                  <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3" for="category">Color <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9">
                          <input type="text" id="color" name="color" required="required" class="form-control col-md-7">
                        </div>
                        @if ($errors->has('color'))
            						  <span class="help-block">
            								<strong>{{ $errors->first('color') }}</strong>
            							</span>
            					  @endif
                      </div>
                    </div>
                    </div>
                    <br />
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-md-offset-5">
                        <button type="submit" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

          <script type="text/javascript">
            $(document).ready(function() {
              $('#birthday').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include('AdminDash.partials._footnote')
        <!-- /footer content -->

      </div>

    @endsection()
