<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="{{asset('/Gentella/js/bootstrap.min.js')}}"></script>

<!-- bootstrap progress js -->
<script src="{{asset('/Gentella/js/progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('/Gentella/js/nicescroll/jquery.nicescroll.min.js')}}"></script>
<!-- icheck -->
<script src="{{asset('/Gentella/js/icheck/icheck.min.js')}}"></script>
<!-- input mask -->
<script src="{{asset('/Gentella/js/input_mask/jquery.inputmask.js')}}"></script>
<!-- tags -->
<script src="{{asset('/Gentella/js/tags/jquery.tagsinput.min.js')}}"></script>
<!-- switchery -->
<script src="{{asset('/Gentella/js/switchery/switchery.min.js')}}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{asset('/Gentella/js/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/Gentella/js/datepicker/daterangepicker.js')}}"></script>
<!-- richtext editor -->
<script src="{{asset('/Gentella/js/editor/bootstrap-wysiwyg.js')}}"></script>
<script src="{{asset('/Gentella/js/editor/external/jquery.hotkeys.js')}}"></script>
<script src="{{asset('/Gentella/js/editor/external/google-code-prettify/prettify.js')}}"></script>
<!-- select2 -->
<script src="{{asset('/Gentella/js/select/select2.full.js')}}"></script>
<!-- form validation -->
<script type="text/javascript" src="{{asset('/Gentella/js/parsley/parsley.min.js')}}"></script>
<!-- textarea resize -->
<script src="{{asset('/Gentella/js/textarea/autosize.min.js')}}"></script>
<script>
  autosize($('.resizable_textarea'));
</script>
<!-- Autocomplete -->
<script type="text/javascript" src="{{asset('/Gentella/js/autocomplete/countries.js')}}"></script>
<script src="{{asset('/Gentella/js/autocomplete/jquery.autocomplete.js')}}"></script>
<!-- pace -->
<script src="{{asset('/Gentella/js/pace/pace.min.js')}}"></script>
<script type="text/javascript">
  $(function() {
    'use strict';
    var countriesArray = $.map(countries, function(value, key) {
      return {
        value: value,
        data: key
      };
    });
    // Initialize autocomplete with custom appendTo:
    $('#autocomplete-custom-append').autocomplete({
      lookup: countriesArray,
      appendTo: '#autocomplete-container'
    });
  });
</script>
<script src="{{asset('/Gentella/js/custom.js')}}"></script>


<!-- select2 -->
<script>
  $(document).ready(function() {
    $(".select2_single").select2({
      placeholder: "Select a state",
      allowClear: true
    });
    $(".select2_group").select2({});
    $(".select2_multiple").select2({
      maximumSelectionLength: 4,
      placeholder: "With Max Selection limit 4",
      allowClear: true
    });
  });
</script>
<!-- /select2 -->
<!-- input_mask -->
<script>
  $(document).ready(function() {
    $(":input").inputmask();
  });
</script>
<!-- /input mask -->
<!-- input tags -->
<script>
  function onAddTag(tag) {
    alert("Added a tag: " + tag);
  }

  function onRemoveTag(tag) {
    alert("Removed a tag: " + tag);
  }

  function onChangeTag(input, tag) {
    alert("Changed a tag: " + tag);
  }

  $(function() {
    $('#tags_1').tagsInput({
      width: 'auto'
    });
  });
</script>
<!-- /input tags -->
<!-- form validation -->
<script type="text/javascript">
  $(document).ready(function() {
    $.listen('parsley:field:validate', function() {
      validateFront();
    });
    $('#demo-form .btn').on('click', function() {
      $('#demo-form').parsley().validate();
      validateFront();
    });
    var validateFront = function() {
      if (true === $('#demo-form').parsley().isValid()) {
        $('.bs-callout-info').removeClass('hidden');
        $('.bs-callout-warning').addClass('hidden');
      } else {
        $('.bs-callout-info').addClass('hidden');
        $('.bs-callout-warning').removeClass('hidden');
      }
    };
  });

  $(document).ready(function() {
    $.listen('parsley:field:validate', function() {
      validateFront();
    });
    $('#demo-form2 .btn').on('click', function() {
      $('#demo-form2').parsley().validate();
      validateFront();
    });
    var validateFront = function() {
      if (true === $('#demo-form2').parsley().isValid()) {
        $('.bs-callout-info').removeClass('hidden');
        $('.bs-callout-warning').addClass('hidden');
      } else {
        $('.bs-callout-info').addClass('hidden');
        $('.bs-callout-warning').removeClass('hidden');
      }
    };
  });
  try {
    hljs.initHighlightingOnLoad();
  } catch (err) {}
</script>
<!-- /form validation -->
<!-- editor -->
<script>
  $(document).ready(function() {
    $('.xcxc').click(function() {
      $('#descr').val($('#editor').html());
    });
  });

  $(function() {
    function initToolbarBootstrapBindings() {
      var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
          'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
          'Times New Roman', 'Verdana'
        ],
        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
      $.each(fonts, function(idx, fontName) {
        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
      });
      $('a[title]').tooltip({
        container: 'body'
      });
      $('.dropdown-menu input').click(function() {
          return false;
        })
        .change(function() {
          $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
        })
        .keydown('esc', function() {
          this.value = '';
          $(this).change();
        });

      $('[data-role=magic-overlay]').each(function() {
        var overlay = $(this),
          target = $(overlay.data('target'));
        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
      });
      if ("onwebkitspeechchange" in document.createElement("input")) {
        var editorOffset = $('#editor').offset();
        $('#voiceBtn').css('position', 'absolute').offset({
          top: editorOffset.top,
          left: editorOffset.left + $('#editor').innerWidth() - 35
        });
      } else {
        $('#voiceBtn').hide();
      }
    };

    function showErrorAlert(reason, detail) {
      var msg = '';
      if (reason === 'unsupported-file-type') {
        msg = "Unsupported format " + detail;
      } else {
        console.log("error uploading file", reason, detail);
      }
      $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    };
    initToolbarBootstrapBindings();
    $('#editor').wysiwyg({
      fileUploadError: showErrorAlert
    });
    window.prettyPrint && prettyPrint();
  });
</script>
<!-- Datatables-->
        <script src="{{asset('/Gentella/js/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.bootstrap.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/buttons.bootstrap.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/jszip.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/pdfmake.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/vfs_fonts.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.fixedHeader.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/responsive.bootstrap.min.js')}}"></script>
        <script src="{{asset('/Gentella/js/datatables/dataTables.scroller.min.js')}}"></script>


        <!-- pace -->
        <script src="{{asset('/Gentella/js/pace/pace.min.js')}}"></script>
        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>
<!-- /editor -->
<!-- /footer content -->
