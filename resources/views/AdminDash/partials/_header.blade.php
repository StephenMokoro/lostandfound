<!-- Bootstrap core CSS -->
<link href="{{asset('/Gentella/css/bootstrap.min.css')}}" rel="stylesheet">

<link href="{{asset('/Gentella/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/animate.min.css')}}" rel="stylesheet">

<!-- Custom styling plus plugins -->
<link href="{{asset('/Gentella/css/custom.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/icheck/flat/green.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/maps/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/calendar/fullcalendar.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/progressbar/bootstrap-progressbar-3.3.0.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/calendar/fullcalendar.print.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/floatexamples.css')}}" rel="stylesheet">
  <!-- editor -->
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{{asset('/Gentella/css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/editor/index.css')}}" rel="stylesheet">
<!-- select2 -->
<link href="{{asset('/Gentella/css/select/select2.min.css')}}" rel="stylesheet">
<!-- switchery -->
<link href="{{asset('/Gentella/css/switchery/switchery.min.css')}}" rel="stylesheet">
  <!-- colorpicker -->
<link href="{{asset('/Gentella/css/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
<!-- ion_range -->
<link href="{{asset('/Gentella/css/normalize.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/ion.rangeSlider.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

<link href="{{asset('/Gentella/js/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/js/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/js/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/js/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('/Gentella/js/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet">

<script src="{{asset('/Gentella/js/jquery.min.js')}}" ></script>
<script src="{{asset('/Gentella/js/nprogress.js')}}" ></script>

<!--[if lt IE 9]>
          <script src="../js/ie8-responsive-file-warning.js')}}"></script>
          <![endif]-->

<!-- HTML5 shim and Respond.js')}} for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
          <![endif]-->
