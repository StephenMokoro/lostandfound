@extends('Welcome.main')
@section('content')
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">SULF</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


<!--Modals-->

<div class = "modal fade" id = "claim" role = "dialog">
        <div class= "modal-dialog">
          <div class = "modal-content">
            <div class = "modal-header">
              <div class="modal-body">
                    <div class="menu">
                             <p align="center">Please insert your SU:ID</p>
                                    <form class="form-inline" action="checkid.php" method="post">
                                        <div class="row">
                                           <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-info" type="button">ID:</button>
                                                    </span>
                                                    <input type="text" class="form-control" placeholder="07****" name="_reqid" required="required">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-md btn-primary" name="approve">Continue</button>
                                        </div>
                                    </form>
                    </div>
              </div>
            </div>
          </div>
        </div>
</div>

<!--/End of Modals-->
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Strathmore University Lost and Found</h1>
                <hr>
                <p>Strathmore University Portal to help you recover your Lost and Found items!</p>
                <p style="color:red;">#Disclaimer</p>
                <p>Goods not claimed over a period of <code>90 days</code> shall be <code>disposed</code> as per the <code>Lost & Found Policy!!</code></p>
                <a href="dash" class="btn btn-primary btn-xl page-scroll">Staff/Student Claim!!</a>
                <a href="visitor/claim" class="btn btn-primary btn-xl page-scroll">Visitor Claim!!</a>
            </div>
        </div>
    </header>
@endsection
