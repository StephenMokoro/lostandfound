<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Administrator;
use App\Location;
use App\Area;
use App\Claim;
use Session;
use SoftDeletes;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.areas')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.arearegistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first()->id;
        $this->validate($request, array(
                'location' => 'required|max:255',
                'area' => 'required|max:255',
            ));
          $area = new Area;
          $area->locationid = decrypt($request->location);
          $area->area = $request->area;
          $area->status = '34';
          $area->userid = $id;
          $area->save();

          Session::flash('registered', 'Area/Room has been registered!');
          return redirect()->route('area.index')->withValue($value);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $value = Session::get('suid');
        $this->validate($request, array(
                'area' => 'required|max:255',
            ));
        $area = Area::find(decrypt($id));
        $area->area = $request->area;
        $area->save();
        Session::flash('registered', 'Area has been updated!');
        return redirect()->route('area.index')->withValue($value);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = Session::get('suid');
        $area = Area::find(decrypt($id));
        $area->delete();
        Session::flash('registered', 'Area has been deleted!');
        return redirect()->route('area.index')->withValue($value);
    }
}
