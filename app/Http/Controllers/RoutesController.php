<?php
namespace App\Http\Controllers;
use Session;
use App\Item;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ContributedItems;
use App\ClaimedItems;
use App\Claim;
use App\Administrator;
use App\Brand;
use App\Area;
use App\SubCategories;
use Illuminate\Http\Request;
use Cas;
use Log;
/**
*
*/
class RoutesController extends Controller
{

		// Admin functions
		public function getAdminDash()
		{
			$value = Session::get('suid');
			$item = Item::where('status','=','34')->count();
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			$pending = Claim::where('status','=','34')->count();
			$contributed = ContributedItems::where('status','=','34')->count();
			$rejected = Claim::where('status','=','55')->count();
			$reclaimed = Claim::where('status','=','99')->count();
			return view('AdminDash.dashboard')->withValue($value)->withItem($item)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withContributed($contributed)->withClaim($claim);
		}
		public function getSettings()
		{
			$value = Session::get('suid');
			return view('AdminDash.setting')->withValue($value);
		}
		public function postVisitorClaim(Request $request){
			$this->validate($request, array(
							'category' => 'required|max:255',
							'subcategory' => 'required|max:255',
							'brand' => 'required|max:255',
							'model' => 'max:255',
							'serialnumber' => 'max:255',
							'primarycolor' => 'required|max:255',
							'secondarycolor' => 'required|max:255',
							'description' => 'required|max:255',
							'claimersuid' => 'required|max:255',
							'claimeremail' => 'required|email|max:255',
							'claimerphonenumber' => 'required|max:255',
							'location' => 'required|max:255',
							'area' => 'required|max:255',
							'datelost' => 'required|max:255',
					));
					$claim = new Claim;
					$claim->brand = ($request->brand);
					$claim->subcategory = ($request->subcategory);
					$claim->primarycolorid = decrypt($request->primarycolor);
					$claim->secondarycolorid = decrypt($request->secondarycolor);
					$claim->areaid = ($request->area);
					$claim->studentid =  $request->claimersuid;
					$claim->model = $request->model;
					$claim->serialnumber = $request->serialnumber;
					$claim->description = $request->description;
					$claim->claimersuid = $request->claimersuid;
					$claim->claimeremail = $request->claimeremail;
					$claim->claimerphonenumber = $request->claimerphonenumber;
					$claim->status = 34;
					$claim->viewed = 6;
					$claim->save();

					Session::flash('userregistered', 'Item has been registered as lost!');
					return redirect()->route('visitor.claim');
		}
		public function getVisitorClaim()
		{

			return view('VisitorDash.non_user_claimform');
		}
		public function getSubCategory()
		{
			$value = Session::get('suid');
			$input = Input::get('category');
			$numbers = DB::table('subcategory')
			->where('categoryid', $input)
			->orderBy('number', 'desc')
			->lists('number','number');

			return Response::json($numbers);
		}
		public function getBrand()
		{
			$input = Input::get('subcategory');
			$numbers = DB::table('brand')
			->where('type_id', $input)
			->orderBy('number', 'desc')
			->lists('number','number');

			return Response::json($numbers);
		}
		public function getRejectedClaims()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.rejectedclaims')->withValue($value)->withClaim($claim);
		}
		public function getReclaimed()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.reclaimed')->withValue($value)->withClaim($claim);
		}
		public function getReclaimedItems()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.reclaimeditems')->withValue($value)->withClaim($claim);
		}
		public function getPendingClaims()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.pendingclaims')->withValue($value)->withClaim($claim);
		}

		// User functions
		public function getUserDash()
		{	Log::info("=========Inside getUserDash()==============");		
			if(cas()->isAuthenticated()) {
			Log::info("getUserDash(): CAS=>".Cas()->user());
			Session::put('suid',Cas()->user());
			Log::info("getUserDash(): Session =>".Session::get('suid'));
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			$pending = Claim::where('status','=','34')->where('claimersuid',"=",$value)->count();
			$rejected = Claim::where('status','=','55')->where('claimersuid',"=",$value)->count();
			$reclaimed = Claim::where('status','=','99')->where('claimersuid',"=",$value)->count();
			return view('UserDash.dashboard')->withValue($value)->withPending($pending)->withRejected($rejected)->withReclaimed($reclaimed)->withClaim($claim);
		} else {
			Log::info("getUserDash(): Cas()->user() NOT SET");
			return redirect('/');
		}
		}
		public function getRejectedUserClaims()
		{
			if(Session::has('suid')) {
			$value = Session::get('suid');
			return view('UserDash.rejectedclaims')->withValue($value);
		} else {
			return redirect('userlogin');
		}
		}
		public function getUserReclaimed()
		{
			if(Session::has('suid')) {
			$value = Session::get('suid');
			return view('UserDash.reclaimed')->withValue($value);
		} else {
			return redirect('userlogin');
		}
		}
		public function getSubcat($id)
		{
			$category = $id;
			return SubCategories::where('categoryid', $category)->get();
		}
		public function getSearchBrand($id)
		{
			$subcategory = $id;
			return Brand::where('subcategoryid', $subcategory)->get();
		}
		public function getArea($id)
		{
			$value = Session::get('suid');
			$location = ($id);
			return Area::where('locationid', $location)->get();
		}
		public function getPendingUserClaims()
		{
			if(Session::has('suid')) {
			$value = Session::get('suid');
			return view('UserDash.pendingclaims')->withValue($value);
		} else {
			return redirect('userlogin');
		}
		}
		public function claim()
		{
			$value = Session::get('suid');
			return view('AdminDash.reclaimed')->withValue($value);

		}
		public function getDeletedClaims()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.deletedclaims')->withValue($value)->withClaim($claim);

		}
		public function removependingclaim($claimid)
		{
			$value = Session::get('suid');
			DB::table('claims')
            ->where('id', decrypt($claimid))
            ->update(['status' => '47']);
			return redirect('pendingclaims')->withValue($value);

		}
		public function contributeitem($itemid)
		{
			$value = Session::get('suid');
			DB::table('items')
            ->where('id', decrypt($itemid))
            ->update(['status' => '190']);
			return redirect('contributed')->withValue($value);

		}
		public function contributeditems()
		{
			$value = Session::get('suid');
			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return view('AdminDash.contributed')->withValue($value);

		}
		public function claimant($claimid, $itemid)
		{
			$claimid=$claimid;
			$itemid=$itemid;
			// echo $claimid;
			// echo $itemid;
			$value = Session::get('suid');
			$id = Administrator::where('suid', $value)->first()->id;

			$claimeditem = new ClaimedItems;
			$claimeditem->itemid = $itemid;
			$claimeditem->claimid = $claimid;
			$claimeditem->adminid = $id;
			$claimeditem->status = '34';
			$claimeditem->claimersuid = '0';
			$claimeditem->userid = '1';
			$claimeditem->destination = '0';
			$claimeditem->save();

			DB::table('claims')
            ->where('id', $claimid)
            ->update(['status' => '99']);

			DB::table('items')
            ->where('id', $itemid)
            ->update(['status' => '99']);

			$claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
			return redirect('reclaimeditems')->withClaim($claim)->withValue($value);

		}

}
?>
