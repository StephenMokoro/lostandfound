<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Claim;
use App\Administrator;
use Session;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.registeredadmins')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.adminregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $value = Session::get('suid');
        $id = Administrator::where('suid', $value)->first();
        $this->validate($request, array(
                'suid' => 'required|max:255|unique:administrators',
            ));
            $user = new User;
            $user->name = $request ->suid;
            $user->email = $request ->suid.'@strathmore.edu';
            $user->password = bcrypt(str_random(20));
            $user->save();

            $administrator = new Administrator;
            $administrator->suid = $request ->suid;
            $administrator->username = $request ->suid;
            $administrator->userid = $user ->id;
            $administrator->registererid = $id->id;
            $administrator->status = 59;
            $administrator->save();

            Session::flash('userregistered', 'User '.$request->suid.' has been registered as an administrator!');
          return redirect()->route('admins.index')->withValue($value);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        {
            //
            $value = Session::get('suid');
            $admin = Administrator::find(decrypt($id));
            $admin->delete();
            Session::flash('registered', 'Admin has been deleted!');
            return redirect()->route('admins.index')->withValue($value);
        }
    }
}
