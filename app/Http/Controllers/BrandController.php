<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Claim;
use App\Brand;
use App\Administrator;
use Session;
use SoftDeletes;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.brands')->withValue($value)->withClaim($claim);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $value = Session::get('suid');
        $claim = Claim::where('status','=','34')->where('viewed','=','6')->count();
        return view('AdminDash.brandregistration')->withValue($value)->withClaim($claim);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

          $value = Session::get('suid');
          $id = Administrator::where('suid', $value)->first()->id;
          $this->validate($request, array(
                  'brand' => 'required|max:255',
              ));
            $brand = new Brand;
            $brand->brand = $request->brand;
            $brand->subcategoryid = $request->subcategory;
            $brand->status = '34';
            $brand->userid = $id;
            $brand->save();

            Session::flash('registered', 'Brand has been registered!');
            return redirect()->route('brand.index')->withValue($value);
      }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $value = Session::get('suid');
        $this->validate($request, array(
                'brand' => 'required|max:255',
            ));
            $brand = new Brand;
            $brand->brand = $request->brand;
            $brand->subcategoryid = $request->subcategory;
            $brand->save();

            Session::flash('registered', 'Brand has been Updated!');
            return redirect()->route('brand.index')->withValue($value);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = Session::get('suid');
        $brand = Brand::find(decrypt($id));
        $brand->delete();
        Session::flash('registered', 'Brand has been deleted!');
        return redirect()->route('brand.index')->withValue($value);
    }
}
