$(document).ready(function(){
	$.getJSON("getcategory.php", success = function(data)
	{
		var options="";
		for (var i = 0; i < data.length; i++)
		{
			options += "<option value='"+ data[i].toLowerCase() + "'>" + data[i] + "</option>";
		}
		$("#cat").append(options);
		$("#cat").change();
	});
	$("#cat").change(function(){
		$.getJSON("getsubcategory.php?category=" + $(this).val(),success = function(data)
		{
			var options="";
			for (var i=0; i< data.length; i++)
			{
				options += "<option value'"+data[i].toLowerCase()+"'>" + data[i] + "</option>";
			}
			$("#subcat").html("");
			$("#subcat").append(options);
		});
	$("#subcat").change(function(){
			$.getJSON("getbrands.php?brand="+ $(this).val(), success = function(data){
				var options = "";
				for (var i=0; i< data.length; i++)
				{
					options += "<option value'"+data[i].toLowerCase()+"'>" + data[i] + "</option>";
				}
				$("#brand").html("");
				$("#brand").append(options);
			});
		});
	});
});